//
//  ViewController.swift
//  G66l13
//
//  Created by Ivan Vasilevich on 11/14/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var testBitton: UIButton!
	@IBOutlet weak var greenBox: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let barButtonItem = navigationItem.rightBarButtonItem
//		barButtonItem.image = barButtonItem.image.imagew
//		imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
		testBitton.backgroundColor = .red
	}
	

	@IBAction func buttonPressed(_ sender: UIButton) {
		print("buttonPressed")
	}
	
	@IBAction func panRecognized(_ sender: UIPanGestureRecognizer) {
		sender.view?.center = sender.location(in: view)
	}
	
	@IBAction func tapRecognized(_ sender: UITapGestureRecognizer) {
		print("tapRecognized")
	}
	
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//		greenBox.frame.origin = touches.first!.location(in: view)
	}
}

