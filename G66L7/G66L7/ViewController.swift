//
//  ViewController.swift
//  G66L7
//
//  Created by Ivan Vasilevich on 10/22/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var centerButton: UIButton!
	@IBOutlet weak var imageView: UIImageView!
	
	let kRunCount = "kRunCount"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		var runCount = UserDefaults.standard.integer(forKey: kRunCount)
		print("run# \(runCount)")
		runCount += 1
		UserDefaults.standard.set(runCount, forKey: kRunCount)
		imageProcessing()
	}
	
	func imageProcessing() {
		let img = UIImage.init(named: "skul")
		print(img)
		imageView.image = img
		
	}

	@IBAction func button1Pressed(_ sender: Any) {
		centerButton.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
											   green: CGFloat(arc4random_uniform(256))/255,
											   blue: CGFloat(arc4random_uniform(256))/255,
											   alpha: 1)
		view.backgroundColor = UIColor.init(hexString: "#f200ff")
		
	}
	
}



