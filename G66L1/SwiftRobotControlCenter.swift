//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

/*
1 что такое робот и как с ним работать
2 операторы условного перехода if - else
3 циклы for - while
4 что такое декомпозиция
5 что делать в дз
6 передать все что писал blackice
*/

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	//in this function change levelName
	override func viewDidLoad() {
		levelName = "L4H" // level name
		
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		while frontIsClear {
			put()
			move()
		}
		put()
		turnLeft()
		
		
	}
	
	func whileLoopExample() {
		while noCandyPresent {
			if frontIsClear {
				move()
			}
				//			else {
				//				break
			else {
				break
			}
		}
	}
	
	// dkshajkdhakshdahkjdshajk
	func forLoopExample() {
		for _ in 0..<14 {
			move()
			put()
		}
	}
	
	func ifElseExample() {
		if frontIsClear {
			l0c()
		}
		else {
			turnRight()
			turnRight()
		}
		
		turnLeft()
		turnRight()
	}
	
	func l0c() {
		move()
		doubleMove()
		
		pick()
		doubleMove()
		turnRight()
		move()
		put()
		turnLeft()
		doubleMove()
	}
	
	func turnLeft() {
		turnRight()
		turnRight()
		turnRight()
	}
	
	func doubleMove() {
		move()
		move()
	}
}
