// 	https://docs.parseplatform.org/ios/guide/
//  MasterViewController.swift
//  G66L12
//
//  Created by Ivan Vasilevich on 11/12/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFObject]()


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showCarAddAlert))
		
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		fetchCars()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		checkLogin()
	}


	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = NSDate()
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel?.text = object["model"] as? String ?? "nil"
		cell.detailTextLabel?.text = object["mark"] as? String ?? "nil"
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}

	@objc func showCarAddAlert() {
//		UIDevice.current.userInterfaceIdiom ==
		let alert = UIAlertController(title: "Add new car", message: "POTOMUSHO", preferredStyle: .alert)
		alert.addTextField { (textField1) in
			textField1.placeholder = "Manufacturer"
		}
		alert.addTextField { (textField2) in
			textField2.placeholder = "Model"
		}
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (act) in
			if let  mark = alert.textFields?.first?.text {
				if let model = alert.textFields?.last?.text {
//					self.addCar(model: <#T##String#>, mark: <#T##String#>)
				}
			}
			
			guard let mark = alert.textFields?.first?.text else {
				return
			}
			
			guard let model = alert.textFields?.last?.text else {
				return
			}
			
			self.addCar(model: model, mark: mark)
			
		}))
		present(alert, animated: true, completion: nil)
	}
	
	func addCar(model: String, mark: String) {
		guard let currentUser = PFUser.current() else {
			return
		}
		let car = PFObject(className: "Car")
		car["model"] = model
		car["mark"] = mark
		car["owner"] = currentUser
		car.saveEventually { (success, error) in
			self.fetchCars()
		}
	}
	
	func fetchCars() {
		guard let currentUser = PFUser.current() else {
			return
		}
		let query = PFQuery(className: "Car")
//		query.whereKey("owner", equalTo: currentUser)
		query.findObjectsInBackground { (objects, error) in
			guard let nonOptionalArray = objects else {
				return
			}
			self.objects = nonOptionalArray
			self.tableView.reloadData()
			let model = nonOptionalArray.last?["model"] as? String ?? "nil"
			print("firs model we get from DB", model)
		}
	}
	

}

extension MasterViewController: PFLogInViewControllerDelegate {
	
	func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
		print("welcome", user.username!)
		navigationItem.title = user.username!
		dismiss(animated: true, completion: nil)
	}
	
	func checkLogin() {
		
		if PFUser.current() == nil {
			// Show the signup or login screen
			let loginVC = PFLogInViewController()
			loginVC.delegate = self
			loginVC.signUpController?.delegate = self
			//			loginVC.de
			present(loginVC, animated: true, completion: nil)
		}
//		PFUser.logOut()
	}
	
	@objc func showFacebookLogin(from viewController: UIViewController) {
		print("fb button pressed")
		
	}
	
}


extension MasterViewController: PFSignUpViewControllerDelegate {
	
	func signUpViewController(_ signUpController: PFSignUpViewController, didSignUp user: PFUser) {
		log(PFLogInViewController(), didLogIn: user)
	}
}
