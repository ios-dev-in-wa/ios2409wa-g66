//
//  AppDelegate.swift
//  G66L12
//
//  Created by Ivan Vasilevich on 11/12/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		let splitViewController = window!.rootViewController as! UISplitViewController
		let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
		navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
		splitViewController.delegate = self
		
		//		j119197@nwytg.net
		let config = ParseClientConfiguration { (conf) in
			conf.clientKey = "yP5EIxE5WM50iwdECUchqF9oPEaLk00vekmvSpR0"
			conf.applicationId = "ZPDL1gFL806QVpgb2NOicVb4KIEiCCcPxmDsTega"
			conf.server = "https://parseapi.back4app.com"
			//			conf.isLocalDatastoreEnabled = true
		}
		Parse.initialize(with: config)
		
		return true
	}


	// MARK: - Split view

	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
	    guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
	    guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
	    if topAsDetailController.detailItem == nil {
	        // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
	        return true
	    }
	    return false
	}

}

