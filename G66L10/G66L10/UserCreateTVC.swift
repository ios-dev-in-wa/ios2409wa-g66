//
//  UserCreateTVC.swift
//  G66L10
//
//  Created by Ivan Vasilevich on 11/5/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class UserCreateTVC: UITableViewController {

	@IBOutlet weak var avatarImageView: UIImageView!
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var surnameTextField: UITextField!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
	}

	@IBAction func selectPicture() {
		let controller = UIImagePickerController()
		controller.sourceType = .camera
		controller.delegate = self
		present(controller, animated: true, completion: nil)
	}
}

extension UserCreateTVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
		info: [UIImagePickerController.InfoKey : Any]) {
		let img = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
		print(img)
		avatarImageView.image = img
		picker.dismiss(animated: true, completion: nil)
	}
}
