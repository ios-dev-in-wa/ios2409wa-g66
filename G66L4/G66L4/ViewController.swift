//
//  ViewController.swift
//  G66L4
//
//  Created by Ivan Vasilevich on 10/10/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		print("\n\n\n\n\n\n\n\n\n")
//		stringName()
//		arrays()
//		optionalBinding()
//		optionalDefaultValue()
		optionalForceUnwrap()
	}
	
	func stringName() {
		let name = "\"I\"van"
		let surname = "KROSAFCHEG"
		let fullName = name + " " + surname
		
		let output = "My name is: \(fullName)"
		print(output)
		let story = """
//
//  ViewController.swift
	//  G66"L4
//
//  Created by Ivan Vasilevich on 10/10/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//
"""
		print("story consist of \(story.count) characters")
		
		var chapter1 = story.prefix(45)
		chapter1.removeFirst()
		print("\n\n\n\n\n\n\n\n\n")
		print(chapter1)
		let nsChapter1 = chapter1 as NSString

		//		let someInt = Int.init()
		
		let someRange = NSRange.init(location: 3, length: 8)
		let partOfCh1 = nsChapter1.substring(with: someRange)
		
		print(partOfCh1)
		
	}
	
	func arrays() {
		let a = 8
//		Array<T> / [T]
		var numbers = [1, 5, a, 19, 666]
		let strNubers = ["1", "5", "a", "19", "666", ""]
		print(numbers)
		print(strNubers)
		print(strNubers[2] + strNubers[3] )
		
		for i in 0..<strNubers.count {
			let strFromArray = strNubers[i]
			print("index #\(i) - element \(strFromArray)")
		}
		
		print(numbers)
//		numbers.insert(111, at: 2)
		numbers[2] = 999
		numbers.remove(at: 2)
		print(numbers)
		numbers.removeAll()
		for _ in 0..<20 {
			let randomNumber = Int(arc4random()%11)
			numbers.append(randomNumber)
		}
		
		print(numbers.sorted())
		
		
	}

	func optionalDefaultValue() {
		let str = "15"
		let addition = 8
		let divider = 2
		let intFromString = Int(str)
		
		let result = ((intFromString ?? 0) + addition) / divider
		print(result)
	}
	
	func optionalBinding() {
		let str = "5"
		let addition = 8
		let divider = 2
		let optionalIntFromString = Int(str)
		if let realInt = optionalIntFromString {
			let result = (realInt + addition) / divider
			print(result)
		}
		else {
			print("enter correct number")
		}
		
	}
	
	func optionalForceUnwrap() {
		let str = "15"
		let addition = 8
		let divider = 2
		let intFromString = Int(str)
		
		let result = (intFromString!  + addition) / divider
		print(result)
	}
	
	
}

