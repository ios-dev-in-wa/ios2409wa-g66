//
//  YAZZTableViewCell.m
//  G66L11P2
//
//  Created by Ivan Vasilevich on 11/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

#import "YAZZTableViewCell.h"

@implementation YAZZTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
