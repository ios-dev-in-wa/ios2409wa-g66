//
//  CustomTVCell.swift
//  G66L11P2
//
//  Created by Ivan Vasilevich on 11/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class CustomTVCell: UITableViewCell {

	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var thumbnailImageView: UIImageView!
	

}
