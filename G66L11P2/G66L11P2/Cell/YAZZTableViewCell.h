//
//  YAZZTableViewCell.h
//  G66L11P2
//
//  Created by Ivan Vasilevich on 11/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YAZZTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
