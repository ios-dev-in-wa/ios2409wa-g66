//
//  File.swift
//  3DQAZAQSTAN
//
//  Created by Alex on 24.07.17.
//  Copyright © 2017 Azat Shauyeyev. All rights reserved.
//

import Foundation
import UIKit



extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    func roundCorners() {
        self.cornerRadius = self.bounds.height/2
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

extension UISegmentedControl {
    func removeBorders() {
        setBackgroundImage(imageWithColor(color: backgroundColor!), for: .normal, barMetrics: .default)
        setBackgroundImage(imageWithColor(color: tintColor!), for: .selected, barMetrics: .default)
        setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}

extension String {
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("Base", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "i18n_language")
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

class LocalizedLabel : UILabel {
    override func awakeFromNib() {
        if let text = text {
            self.text = text.localized
        }
    }
}


class LocalizebleButton: UIButton {
    override func awakeFromNib() {
        if let title = title(for: .normal) {
            setTitle(title.localized, for: .normal)
        }
    }
}

extension UIImage
{
    func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension Dictionary {
    var urlParams: String {
        return self.getUrlParams(includeQuestionMark: true)
    }
    
    func getUrlParams(includeQuestionMark: Bool) -> String {
        var result = includeQuestionMark ? "?" : ""
        for (key, value) in self {
            result += "\(key)=\(value)&"
        }
        return result
    }
    
}


extension CALayer {
	
	func colorOfPoint(point:CGPoint) -> CGColor {
		
		var pixel: [CUnsignedChar] = [0, 0, 0, 0]
		
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
		
		let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
		
		context!.translateBy(x: -point.x, y: -point.y)
		
		self.render(in: context!)
		
		let red: CGFloat   = CGFloat(pixel[0]) / 255.0
		let green: CGFloat = CGFloat(pixel[1]) / 255.0
		let blue: CGFloat  = CGFloat(pixel[2]) / 255.0
		let alpha: CGFloat = CGFloat(pixel[3]) / 255.0
		
		let color = UIColor(red:red, green: green, blue:blue, alpha:alpha)
		
		return color.cgColor
	}
}

extension UIImageView{
	func frameForImageInImageViewAspectFit() -> CGRect
	{
		if  let img = self.image {
			let imageRatio = img.size.width / img.size.height;
			
			let viewRatio = self.frame.size.width / self.frame.size.height;
			
			if(imageRatio < viewRatio)
			{
				let scale = self.frame.size.height / img.size.height;
				
				let width = scale * img.size.width;
				
				let topLeftX = (self.frame.size.width - width) * 0.5;
				
				return CGRect(x:topLeftX, y:0, width:width, height:self.frame.size.height);
			}
			else
			{
				let scale = self.frame.size.width / img.size.width;
				
				let height = scale * img.size.height;
				
				let topLeftY = (self.frame.size.height - height) * 0.5;
				return CGRect(x:0, y:topLeftY, width:self.frame.size.width, height:height);
			}
		}
		
		return CGRect.zero
	}
}

extension CGColor {
	var hexString: String {
		var r = 0, g = 0, b = 0, a = 0;
		r = Int(components![0] * 255)
		g = Int(components![1] * 255)
		b = Int(components![2] * 255)
		a = Int(components![3] * 255)
		return NSString(format: "%02x%02x%02x%02x", r, g, b, a) as String
	}
}

extension Date {
    func dateStrFrom() -> String {
        let formatter = DateFormatter()
        var dateStr = ""
        
        formatter.dateStyle = .medium
        formatter.doesRelativeDateFormatting = true
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "H:mm"
        
        dateStr = "\(formatter.string(from: self))"
        
        if Calendar.current.isDateInYesterday(self) || Calendar.current.isDateInToday(self) {
            dateStr += " в ".localized + "\(timeFormatter.string(from: self))"
        }
        
        return dateStr
    }
}

