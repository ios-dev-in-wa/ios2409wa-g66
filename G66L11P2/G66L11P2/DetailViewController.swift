//
//  DetailViewController.swift
//  G66L11P2
//
//  Created by Ivan Vasilevich on 11/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!
	@IBOutlet weak var scroller: EHHorizontalSelectionView!
	

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.description
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		scroller.delegate = self as! EHHorizontalSelectionViewProtocol
		// Do any additional setup after loading the view, typically from a nib.
		configureView()
	}

	var detailItem: NSDate? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

extension DetailViewController: EHHorizontalSelectionViewProtocol {
	func numberOfItems(inHorizontalSelection hSelView: EHHorizontalSelectionView) -> UInt {
		return 2
	}
	
	func titleForItem(at index: UInt, forHorisontalSelection hSelView: EHHorizontalSelectionView) -> String? {
		let ind = Int(index)
		return ["qwertyuiop", "YAAZZZZZ"][ind]
	}
}

