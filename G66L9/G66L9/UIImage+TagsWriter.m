//
//  UIImage+TagsWriter.m
//  G66L9
//
//  Created by Ivan Vasilevich on 10/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

#import "UIImage+TagsWriter.h"

@implementation UIImage_TagsWriter

+ (void) readTagsFromInfo: (NSDictionary *)info {
	
	NSMutableDictionary *metadata = info[UIImagePickerControllerMediaMetadata];
	 UIImage *image = info[UIImagePickerControllerOriginalImage];
	// set image name and keywords in IPTC metadata
	NSString *iptcKey = (NSString *)kCGImagePropertyIPTCDictionary;
	NSMutableDictionary *iptcMetadata = metadata[iptcKey];
	iptcMetadata[(NSString *)kCGImagePropertyIPTCObjectName] = @"Image Title";
	iptcMetadata[(NSString *)kCGImagePropertyIPTCKeywords] = @"some keywords";
	metadata[iptcKey] = iptcMetadata;
	
	// set image description in TIFF metadata
	NSString *tiffKey = (NSString *)kCGImagePropertyTIFFDictionary;
	NSMutableDictionary *tiffMetadata = metadata[tiffKey];
	tiffMetadata[(NSString *)kCGImagePropertyTIFFImageDescription] = @"Description for image"; // only visible in iPhoto when IPTCObjectName is set
	metadata[tiffKey] = tiffMetadata;
	
	// save image to camera roll
//	ALAssetsLibrary library = [[ALAssetsLibrary alloc] init];
//	[library writeImageToSavedPhotosAlbum:image.CGImage metadata:metadata completionBlock:nil];
}


@end
