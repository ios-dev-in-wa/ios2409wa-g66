//
//  ViewController.swift
//  G66L9
//
//  Created by Ivan Vasilevich on 10/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet var inputFields: [UITextField]!
	@IBOutlet weak var loginTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		var image = UIImage(named: "1")!
		print(image)
		
		//		loginTextField.delegate = self
		passwordTextField.delegate = self
		
		for textField in inputFields {
			textField.delegate = self
		}
		
		
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
//		let storyB = UIStoryboard.init(name: "Main", bundle: nil)
//		let greenVC = storyB.instantiateViewController(withIdentifier: "GreenVC")
//		present(greenVC, animated: true, completion: nil)
		
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let picker = UIImagePickerController.init()
		picker.delegate = self
		present(picker, animated: true, completion: nil)

	}
}

extension ViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		print(textField.tag)
		if textField == loginTextField {
			passwordTextField.becomeFirstResponder()
		}
		else {
			textField.resignFirstResponder()
			view.endEditing(true)
		}
		return false
	}
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		
		picker.dismiss(animated: true, completion: nil)
		
		UIImage_TagsWriter.readTags(fromInfo: info)
	}
}

