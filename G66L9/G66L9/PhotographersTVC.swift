//
//  PhotographersTVC.swift
//  G66L9
//
//  Created by Ivan Vasilevich on 10/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class PhotographersTVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

	@IBOutlet weak var tableView: UITableView!
	
	let words =  """
Created by Ivan Vasilevich on 10/29/18  Copyright © 2018 RockSoft. All rights reserved.
""".components(separatedBy: " ")
	
	// MARK: - ViewControllerLifeCycle
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		tableView.dataSource = self
		tableView.delegate = self
    }
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return words.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		let wordForCell = words[indexPath.row]
		cell.textLabel?.text = wordForCell
		return cell
	}
	
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedWord = words[indexPath.row]
		title = selectedWord
	}
	
	
}
