//
//  UIImage+TagsWriter.h
//  G66L9
//
//  Created by Ivan Vasilevich on 10/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage_TagsWriter : NSObject

+ (void) readTagsFromInfo: (NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
