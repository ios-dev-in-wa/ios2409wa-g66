//
//  ViewController.swift
//  G66L14
//
//  Created by Ivan Vasilevich on 11/19/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var labelWidthConstraint: NSLayoutConstraint!
	
	var label66: UILabel! {
		return view.viewWithTag(66) as? UILabel
	}
	var label55: UILabel! {
		return view.viewWithTag(55) as? UILabel
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}
	
	
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		guard let pos = touches.first?.location(in: view) else {
			return
		}
		
		UIView.animate(withDuration: 1) {
			
		}
		
//		UIView.animate(withDuration: 1, animations: {
//			label?.center = pos
//			self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
//												green: CGFloat(arc4random_uniform(256))/255,
//												blue: CGFloat(arc4random_uniform(256))/255,
//												alpha: 1)
//		}) { (complete) in
//			if complete {
//				label?.isHidden = !(label?.isHidden ?? true)
//			}
//		}
//		view.layoutIfNeeded()
//
//		labelWidthConstraint.constant = CGFloat.random(in: 100...300)
//
//		label55.setNeedsUpdateConstraints()
//
//		UIView.animateKeyframes(withDuration: 1, delay: 0, options: [ .allowUserInteraction], animations: {
//			self.label66?.center = pos
//			self.label55.superview?.layoutIfNeeded()
//			self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
//												green: CGFloat(arc4random_uniform(256))/255,
//												blue: CGFloat(arc4random_uniform(256))/255,
//												alpha: 1)
//		}) { (complete) in
//			if complete {
////				label?.isHidden = !(label?.isHidden ?? true)
//			}
//		}
		randomText()
	}
	
	func randomText() {
		UIView.transition(with: view, duration: 2.33, options: .transitionCrossDissolve, animations: {
			self.label55.text = Int.random(in: 0...999999999).description
			self.label66.text = Int.random(in: 0...999999999).description
		}, completion: nil)
		
	}


}

class Mac {
	static var OSType = "MACOS"
}
