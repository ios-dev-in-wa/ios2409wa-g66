//
//  ViewController.swift
//  G66L6
//
//  Created by Ivan Vasilevich on 10/17/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

enum AnimalType: Int {
	case cat = 0
	case dog
	case ondatra
}

class ViewController: UIViewController {
	
	@IBOutlet weak var statusLabel: UILabel!
	
	let p = CGPoint.init(x: 5, y: 10)
	var count = 0

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let myCat: AnimalType = .cat
		print(myCat.rawValue)
		let randomNumber = Int.random(in: 0..<2)
		let randAnimal = AnimalType.init(rawValue: randomNumber)!
		print(randAnimal)
		switch randAnimal {
		case .dog:
			print("3 time per day")
		case .cat:
			print("0 time per day")
		case .ondatra:
			print("99 time per day")
//		default:
//			print("fhdskjf")
			
			
			
		}
		foo()
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		count += 1
		print(count)
		drawBox()
	}
	
	func foo() {
		let dict = ["one": 1]//["a", "abcd", "eeeeeeeeee"]
		for str in dict {
//			str.key
			print(str)
		}
//		let nsDict = dict as NSDictionary
//		nsDict.write(toFile: "/Users/ivanvasilevich/Desktop/Peaky.plist", atomically: false)
		
		let nsDictFromFile = NSDictionary.init(contentsOfFile: "/Users/ivanvasilevich/Desktop/Peaky.plist")!
		print(nsDictFromFile)
//		print(nsDictFromFile as? [String : Int])
		
		
		let arr2 = [1, 2, 3]
		
		for (num, i) in arr2.enumerated() {
			print(i, num)
		}
		
		var name: String? = "Ivan"
		let c = name?.count
		print(c!)
		name = nil
		
	}
	
	func drawBox() {
		let rect = CGRect.init(x: 10, y: 150, width: 200, height: 80)
		let box = UIView.init(frame: rect)
		print(box.frame)
		box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									  green: CGFloat(arc4random_uniform(256))/255,
									  blue: CGFloat(arc4random_uniform(256))/255,
									  alpha: 1)
		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									   green: CGFloat(arc4random_uniform(256))/255,
									   blue: CGFloat(arc4random_uniform(256))/255,
									   alpha: 1)
		view.addSubview(box)
		
	}

	@IBAction func buttonPressed() {
		print("blah blah..")
		buttonPressed2()
	}
	
	@IBAction func buttonPressed2() {
		
		statusLabel.text = "blah blah.."
	}
	
	@IBAction func digitPressed(_ sender: UIButton) {
		print(sender.tag)
	}
	
}

