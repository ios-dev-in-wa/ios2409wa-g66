//
//  ViewController.swift
//  G66L5
//
//  Created by Ivan Vasilevich on 10/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let a = foo(n: 4)
		print("a = \(a)")
		dictionary(keyValuePair: (key: "111", value: "SuperSluzba"))
		playWithBulding()
	}
	
	func foo(n: Int) -> Int {
		let result = n + 5
		return result
	}
	
	func dictionary(keyValuePair: (key: String, value: String)) {
		var dict = ["101": "Fire",
					"102": "Police",
					"103": "Ambulance"]

//		keyValuePair.key
		dict["104"] = "Gas"
		dict["101"] = "Gas"
		dict.removeValue(forKey: "101")
		let fire = dict["101"]
		
		let candy1 = ["color": "yellow",
					  "size": "medium",
					  "owner": "Nikita"]
		let candy2 = ["color": "yellow",
					  "size": "medium",
					  "owner": "Nikita"]
//		print([fire!, candy1, candy2])
	}

	func playWithBulding() {
		let myBuild = Building(freeApt: 255, startingMoney: 10_000)
		myBuild.addRoaches()
		myBuild.sellApartment()
//		myBuild.fireAlarm()
		print(myBuild.description)
	}
	
}

