//
//  Building.swift
//  G66L5
//
//  Created by Ivan Vasilevich on 10/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Building: NSObject {

	var freeAppartments = 100
	var money: Int
	var rentalAggrement: Bool = false
	var roaaches = 0
	
	override var description: String {
		var result = super.description
		result += "free apartments = \(freeAppartments)\n"
		result += "roaches = \(roaaches)\n"
		return result
	}
	
	func sellApartment() {
		
	}
	
	private func fireAlarm() {
		print("fireAlarm")
	}
	
	func addRoaches() {
		for _ in 0..<freeAppartments {
			roaaches += 1
		}
	}
	
	init(startingMoney: Int) {
		money = startingMoney
	}

	init(freeApt: Int) {
		money = 100
		freeAppartments = freeApt
	}
	
	init(freeApt: Int, startingMoney: Int) {
		money = startingMoney
		freeAppartments = freeApt
	}

}
