//
//  ViewController.swift
//  G66L3
//
//  Created by Ivan Vasilevich on 10/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		printSomething()
		casting()
		let studentsCount: UInt32 = 13
		randomPerson(countOfOptions: studentsCount)
		operators()
		switchExamle(studentNumber: arc4random()%studentsCount)
		bar()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let studentsCount: UInt32 = 13
		switchExamle(studentNumber: arc4random()%studentsCount)
	}
	
	func pirintSomething() {
		let a = 5
		let b = 11
		let c = a + b
		if a > b {
			print(a)
			let newA = 55
			print(newA)
		}
		print(a)
		print("Do any additional setup after loading the view, typically from a nib")
		print("c =", c)
//		print("c =", c, ", b =", b)
		print("c = \(c)")
		print("\(a) + \(b) = \(c)")
	}
	
	func casting() {
		let a: Int = 51
		let pi = 3.14159
		let c = Double(a) + pi
		print("c = Double(a) + pi.rounded()", c)
	}
	
	func randomPerson(countOfOptions: UInt32) {
		var optionNumber: UInt32 = 0
		optionNumber = arc4random()%countOfOptions
		optionNumber = optionNumber + 1
		print("choose option #\(optionNumber)")
	}
	
	func operators() {
//		+ - * / %
		let a = -5
		let b = +a
		print("a = \(a)")
		print("b = \(b)")
		
		// < > <= >= == !=
		let mnogoKotyat = arc4random()%20 > 10
		if mnogoKotyat {
			print("Bery kotyat i yed6 na Darnitsa station")
			
		}
		else {
			print("no need to go to Darnitsa station!!!!!")
		}
		
		
		let maloLave = arc4random()%200 < 80
		
		if !mnogoKotyat || maloLave {
			print("no need to go to Darnitsa station!!!!!")
		}
		
		if mnogoKotyat || !maloLave {
			print("no need to go to Darnitsa station!!!!!")
		}
		
		if mnogoKotyat && !maloLave {
			print("no need to go to Darnitsa station!!!!!")
		}
		
		//		+ - * / %
		var g = 88
		g = g + 4
		g += 4
		g = 14
		g = g % 5
		g %= 5
//		print(abs(-4.555))
//		14 % 5 = 4
//		14 / 5 = 2
	}

	func cascadeIfCondition() {
		let mnogoKotyat = arc4random()%20 > 10
		if mnogoKotyat {
			
		}
		else if mnogoKotyat && mnogoKotyat {
			
		}
		else {
			
		}
	}
	
	func switchExamle(studentNumber: UInt32) {
//		if studentNumber == 1 {
//			print("Oleg", terminator : "")
//		}
//		else if studentNumber == 2 {
//			print("Myroslav", terminator : "")
//		}
//		else if studentNumber == 3 {
//			print("Danil", terminator : "")
//		}
//		else if studentNumber == 4 {
//			print("Igor", terminator : "")
//		}
//		else {
//			print("Ira", terminator : "")
//		}
		
		switch studentNumber {
		case 1:
			print("Oleg", terminator : "")
		case 2:
			print("Myroslav", terminator : "")
		case 3:
			print("Danil", terminator : "")
		case 4:
			print("Igor", terminator : "")
		case 5:
			print("Ann", terminator : "")
		case 6, 7, 8, 9:
			print("Andrey", terminator : "")
		default:
			print("Ira", terminator : "")
		}
		
		print(" winner")
	}

	func bar() {
		
		for i in 0..<10 {
			if i == 7 {
				continue
			}
			
			print("hello #\(i + 1)")
		}
		
		for i in 0..<10 {
			for j in 0..<10 {
			
				print("i = \(i + 1) \t j = \(j + 1)")
				
			}
			
			
		}
	}
}

