//
//  MasterViewController.swift
//  G66L15
//
//  Created by Ivan Vasilevich on 11/21/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [Any]()


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}
	
	let fileManager = FileManager.default
	
	func imageWithDate(date: Date) -> UIImage? {
		let name = "\(Int(date.timeIntervalSince1970))"
		let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
		let fileURL = documentDirectory.appendingPathComponent(name)
		let data = try! Data.init(contentsOf: fileURL)
		return UIImage(data: data)
	}
	
	func imageDataWithDate(date: Date) -> Data? {
		let name = "\(Int(date.timeIntervalSince1970))"
		let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
		let fileURL = documentDirectory.appendingPathComponent(name)
		let data = try? Data.init(contentsOf: fileURL)
		return data
	}

	@objc
	func insertNewObject(_ sender: Any) {
		let image = UIImage(named: "\(Int.random(in: 0...2))")!
		let data = image.jpegData(compressionQuality: 0.1)!
		let date = Date()
		let name = "\(Int(date.timeIntervalSince1970))"
		do {
			let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
			let fileURL = documentDirectory.appendingPathComponent(name)
			
			try data.write(to: fileURL)
			
			print(fileURL)
			
		} catch {
			print(error.localizedDescription)
		}
		
//		do {
//			try <#throwing expression#>
//		} catch <#pattern#> {
//			<#statements#>
//		}
		
		objects.insert(date as NSDate, at: 0)
		let indexPath = IndexPath(row: 0, section: 0)
		tableView.insertRows(at: [indexPath], with: .automatic)
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		saveData()
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row] as! NSDate
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row] as! Date
		cell.textLabel!.text = object.description
		cell.imageView?.image = imageWithDate(date: object)
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}

	func saveData() {
		if let dates = objects as? [Date] {
			var objetctsToWrite = NSMutableArray()
			for singleDate in dates {
				let point = ["date" : singleDate,
							 "image" : imageDataWithDate(date: singleDate)!] as [String : Any]
				objetctsToWrite.add(point)
			}
			let documentDirectory = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
			let fileURL = documentDirectory.appendingPathComponent("DataOfApp")
			try! objetctsToWrite.write(to: fileURL)
			
		}
	}

}

